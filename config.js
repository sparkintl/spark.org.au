var url = require('url');

var publicPort = (process.env.PUBLIC_PORT || process.env.PORT || 9000);
var baseUri = (process.env.BASE_URI || 'http://localhost') +
            (publicPort === 80 ? '' : ':' + publicPort);

var redisUrl = process.env.REDISTOGO_URL || process.env.REDIS_URL;
redisUrl = redisUrl ? url.parse(redisUrl) : {};

var braintreeEnv = process.env.BT_ENV || 'live';
if (braintreeEnv === 'sandbox') {
    console.log('Braintree Sandbox values being used.');
    var braintree = {
        environment : braintreeEnv,
        merchantId : process.env.BT_SANDBOX_MERCHANT_ID || console.log('Braintree merchantId not set (sandbox on)'),
        publicKey : process.env.BT_SANDBOX_PUBLIC_KEY || console.log('Braintree public key not set (sandbox on)'),
        privateKey : process.env.BT_SANDBOX_PRIVATE_KEY || console.log('Braintree private key not set (sandbox on)'),
        clientSideEncryptionKey : process.env.BT_SANDBOX_CLIENT_ENCRYPTION_KEY || console.log('Braintree client encryption key not set (sandbox on)')
    };
} else {
    var braintree = {
        environment : braintreeEnv,
        merchantId : process.env.BT_MERCHANT_ID || console.log('Braintree merchantId not set'),
        publicKey : process.env.BT_PUBLIC_KEY || console.log('Braintree public key not set'),
        privateKey : process.env.BT_PRIVATE_KEY || console.log('Braintree private key not set'),
        clientSideEncryptionKey : process.env.BT_CLIENT_ENCRYPTION_KEY || console.log('Braintree client encryption key not set')
    };
}

module.exports = {
    // in `production` things will be less verbose
    environment: process.env.NODE_ENV || 'development',
    // HTTP port to listen on
    port: process.env.PORT || 9000,
    // no options mean connect to localhost
    // TODO: work out how we connect to the heroku redis provider
    redis: {
        host : redisUrl.host,
        port : redisUrl.port,
        pass : redisUrl.auth && redisUrl.auth.split(':')[1]
    },
    hashingSecret : process.env.HASHING_SECRET || 'now = new Date(); day = now.getDate(); alert(day + 1);',
    sessions: {
        //key: "spark.token",
        secret: process.env.SESSION_SECRET || 'purplemonkeydishwasherbatteryacid',
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 14 // 1000ms * 60s * 60m * 24h * 14d == 2 weeks
        }
    },
    couchdb: process.env.COUCHDB || process.env.CLOUDANT_URL || 'http://localhost:5984',
    facebook: {
        clientID: process.env.FB_APP_ID || console.log('facebook app id not set'),
        clientSecret: process.env.FB_APP_SECRET || console.log('facebook app secret not set'),
        callbackURL: baseUri + '/auth/facebook/callback'
    },
    emailSmtp: {
        service: "Gmail",
        fromAddress : process.env.GMAIL_USERNAME,
        fromName : 'Spark* International', 
        auth: {
            user: process.env.GMAIL_USERNAME || console.log('GMail username not set'),
            pass: process.env.GMAIL_PASSWORD || console.log('GMail password not set')
        }
    },
    salesforce: {
        clientId: process.env.SF_CLIENT_ID || '3MVG9Nvmjd9lcjRneudpgXBcP4txbNNsKKZ9z9f01CSwVqpLpmvc.QHB6mKBeHY_pIOSgY9YGMSvFDtQTPtv5',
        clientSecret: process.env.SF_CLIENT_SECRET || '2632926850905841936',
        // not sur if this matters for server side things
        redirectUri: baseUri + '/oauth/_callback',
        environment: process.env.SF_ENV || 'sandbox',
        username: process.env.SF_USER_NAME || 'web@sparkinternational.org.webtestd',
        password: process.env.SF_PASSWORD || 'Spark8008',
        securityToken: process.env.SF_TOKEN || 'kAPBrtfXrWSSrTlHlrANh1zl'
    },
    braintree : {
        merchantId : braintree.merchantId,
        publicKey : braintree.publicKey,
        privateKey : braintree.privateKey,
        clientSideEncryptionKey : braintree.clientSideEncryptionKey
    }
};

console.log('CouchDB host: ' + url.parse(module.exports.couchdb).host);
console.log('Redis host: ' + module.exports.redis.host);
