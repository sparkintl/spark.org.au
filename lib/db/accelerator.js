module.exports = function(sparkdb) {
    "use strict";

    function getAccelerator(id, callback) {
        sparkdb.get(id, callback);
    }

    function updateOrInsertAccelerator(accelerator, callback) {
        var docid = accelerator._id;
        getAccelerator(docid, function(err, accel) {
            if(err && err.message !== 'missing') {
                return callback(err);
            }
            if(err && err.message === 'missing') {
                accel = blankAccelerator();
            }
            if(!accel) {
                accel = blankAccelerator();
            }
            var keys = Object.keys(accelerator);
            keys.forEach(function(key) {
                if(key === '_rev') {
                    return;
                }
                accel[key] = accelerator[key];
            });
            console.log("saving accelerator %j", accelerator);
            sparkdb.insert(accel, docid, function(err, body) {
                if(err) {
                    console.log("error saving accelerator: %j", err);
                    return callback(err);
                }
                callback(null, accel);
            });
        });
    }

    function saveAccelerator(accelerator, callback) {
        //TODO: validation
        if(!accelerator.type || accelerator.type !== 'accelerator') {
            return callback(new Error("accelerator.type is not set correctly."));
        }
        if (!accelerator.start_date) {
            return callback(new Error("Accelerator start_date is required."));
        }

        // location validation??
        
        console.log("saving accelerator %j", accelerator);
        sparkdb.insert(accelerator, function(err, body) {
            if(err) {
                console.log("error saving accelerator: %j", err);
                return callback(err);
            }
            accelerator._id = body.id;
            callback(null, accelerator);
        });
    }

    function getCount(callback) {
        sparkdb.view('accelerator', 'count', { group : true }, 
            function(err, doc) {
                if(err) {
                    return callback(err);
                }
                callback(null, doc.rows.length && doc.rows[0].value);
            });
    }

    function blankAccelerator() {
        return {
            "type" : "accelerator",
            "start_date" : "",
            "location" : ""
        };
    }

    return {
        count : getCount,
        get : getAccelerator,
        blank : blankAccelerator,
        save : saveAccelerator,
        upsert: updateOrInsertAccelerator
    };
};
