"use strict";

var server;
module.exports = function() {
    if (!server) {
        server = require('http').createServer(function(req, res) {
            res.writeHead(500);
            res.end('The Braintree development server at localhost:3000 wil only ever respond with 500 errors.');
        });
        server.listen(3000);
    }
    return server;
};
