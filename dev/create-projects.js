// This script will create 100 random projects in your database.

var db = require("../lib/db");
var async = require('async');

var categories = ['Education', 'Health', 'Employment', 'Gender Equality'];
var locations= ['Papua New Guinea', 'South Africa', 'Kenya', 'India'];

var stats = {
    contribution : {
        count : {
            mean : 20,
            std_dev : 15
        },
        amount : {
            mean : 25,
            std_dev : 15
        }
    },
    updates : {
        count : {
            mean : 2,
            std_dev : 1
        },
        affected : {
            mean : 20,
            std_dev : 40
        }
    }
};



var ONE_YEAR = 1000 * 60 * 60 * 24 * 365;
var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;

function randomNormal(data) {
    var normal = (Math.random()*2 - 1) + (Math.random()*2 - 1) + (Math.random()*2 - 1);
    return normal * data.std_dev + data.mean;
}

function createChangemaker(i, blank) {
    var changemaker = blank();
    changemaker.first_name = "Sam";
    changemaker.last_name = [65,97,97,97,97].map(function(a) {
        return String.fromCharCode(Math.floor(Math.random() * 26 + a));
    }).join('');
    changemaker.story = "Story of my life! " + Math.random() + ".";
    return changemaker;
}

function createProject(i, changemaker, blank) {
    var project = blank();
    project.name = "Project #" + i;
    project.created_date = Math.floor(Date.now() - ONE_YEAR * 4 * Math.random());
    project.category = categories[Math.floor(Math.random()*4)];
    project.mission = "The " + Math.random() + " thing.";
    project.problem = "Damn " + Math.random() + " thing.";
    project.solution = "Do a " + Math.random() + " thing.";
    project.people = "The " + Math.random() + " people.";
    project.location = locations[Math.floor(Math.random()*4)];
    project.changemaker_id = changemaker._id;

    var max_date = Date.now();
    do {
        var grant = createGrant(!!project.funding_grants.length, max_date);
        project.funding_grants.push(grant);
        max_date = grant.created_date;
    } while(Math.random() > 0.8);

    max_date = Date.now();
    var numUpdates = Math.floor(Math.max(0, randomNormal(stats.updates.count)));
    while(numUpdates--) {
        var update = createProjectUpdate(max_date);
        project.updates.push(update);
        max_date = update.created_date;
    }

    return project;
}

function createGrant(attained, max_date) {
    var attained_date = null;
    if (attained) {
        attained_date = max_date - Math.floor(Math.random() * ONE_YEAR);
        max_date = attained_date;
    }

    var created_date = max_date - Math.floor(Math.random() * ONE_YEAR);

    var grant = {
        amount_usd : 1000,
        created_date : created_date,
        attained_date : attained_date,
        contributions : []
    };

    var numContributions = Math.floor(Math.max(attained_date ? 1 : 0, randomNormal(stats.contribution.count)));
    while(numContributions--) {
        grant.contributions.push({
            user_id : 0,
            is_anonymous : Math.random() > 0.8,
            amount_usd : Math.floor(Math.max(1, randomNormal(stats.contribution.amount))),
            transaction_date : Math.floor(
                    Math.random() * ((attained_date || max_date) - created_date) +
                    created_date
                )
        });
    }

    return grant;
}

function createProjectUpdate(max_date) {
    return {
        "title" : 'The title of the update',
        "body" : "The body of the update.",
        "people_affected" : randomNormal(stats.updates.affected),
        "created_date" : max_date - Math.floor(Math.random() * ONE_WEEK * 4),
        "media" : []
    };
}

function createProjects(api, changemakers, next) {
    var count = 100;
    var saves = [];
    while(count--) {
        var project = createProject(count, changemakers[count], api.project.blank);
        saves.push(api.project.save.bind(null, project));
    }
    async.parallel(saves, function(err, res) {
        if (err) {
            return next(err);
        }

        next(null, res);
    });
}

function createChangemakers(api, next) {
    var count = 100;
    var saves = [];
    while(count--) {
        var changemaker = createChangemaker(count, api.changemaker.blank);
        saves.push(api.changemaker.upsert.bind(null, changemaker));
    }
    async.parallel(saves, function(err, res) {
        if (err) {
            return next(err);
        }

        next(null, res);
    });
}

db.ready(function(api) {
    createChangemakers(api, function(err, changemakers) {
        createProjects(api, changemakers, function(err) {
            if (err) throw err;
            console.log('Created 100 projects.');
        });
    });
});
