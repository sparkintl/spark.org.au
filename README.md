
# spark.org.au

## Important Documents

* ~/doc/coding-standards.md
* ~/doc/directory-structure.md

## Setting up a dev environment

1. Download and install [Git](http://git-scm.com/)
2. Download and install [NodeJS](http://nodejs.org/) and NPM (should come with Node)
3. Install Grunt v0.3.17 globally (a consistent version between developers would be helpful)


    **Note**: at this time grunt is optional. If you cannot get grunt to work please continue unless otherwise instructed.

    ``
    > npm install -g grunt@0.3.17
    ``

4. Clone this repository

    ``
    > git clone git@bitbucket.org:sparkintl/spark.org.au.git
    ``

5. Download all the dependencies for the repo:

    ``
    > cd spark.org.au
    > npm install
    ``

6. Install [CouchDB](http://couchdb.apache.org/) and [redis](http://redis.io/download)

    **Note**: at this time redis is not required for this site to run locally.

    If you're running [Homebrew on OSX](http://github.com/mxcl/homebrew) then you can :

    ``
    > brew install couchdb
    > brew install redis
    ``

    You can also download directly from [couchdb.apache.org](http://couchdb.apache.org/)

And you're set!

## Tools Used

* [ExpressJS](http://expressjs.com/)
* [Jade-lang](http://jade-lang.com/)
* [Stylus](http://learnboost.github.com/stylus/)


## Running tests

**Note**: Up to this point tests have not been written. We would very much like to turn this around. If you have trouble installing the below continue work but inform a member of the Spark* team and we will coordinate with you to get it working.

To run the client-side QUnit tests, download [PhantomJS](http://phantomjs.org/) and add it to your `PATH`

If you're running [Homebrew on OSX](http://github.com/mxcl/homebrew) then you can just `brew install phantomjs`

### Lint and run tests

``
> grunt
``

### Watch for changes and automatically run linting and tests

*You should develop with a watch task running in the background. You will be much more productive.*

``
> grunt watch
``

### Some other handy hints

There are several environment variables utilised - these include :

* `environment: process.env.NODE_ENV || "development"`
* (Session) `secret: process.env.SESSION_SECRET ||
"purplemonkeydishwasherbatteryacid"`
* `couchdb: process.env.COUCHDB || 'http://localhost:5984'`

You shouldn't need to worry about most of these in development.

### Salesforce integration

You will only need to worry about this section if you are working in the projects area (as at 2013.06.04). If you need to get in touch with your Spark* contact and we will hook you up.

### Rules of this repo

* Add QUnit/Nodeunit tests for any new or changed functionality.
* Every change requires a pull request to be approved by another contributor.
* No change will be accepted with failing JSHint or failing tests
* Don't commit any change that breaks `grunt watch`.

## Release History
* v0.1 - launch
* v0.2 - Some extra stuff

## License
Copyright (c) 2012 Spark International
