var nforce = require("./login");
var fs = require('fs');

var queries = JSON.parse(fs.readFileSync(__dirname + "/queries.json"));


nforce.connect(function(err, connectionDetails) {
    if(err) {throw err;}
    var sf = connectionDetails.nforce;
    var oauth = connectionDetails.oauth;
    sf.query(queries.people, oauth, function(err, results) {
        if(err) {
            throw err;
        }
        if(results && results.records) {
            console.log(JSON.stringify(results.records, null, 2));
        } else {
            console.log("no results");
        }
    });
});
