var _ = require("lodash");

// I'm sure there is an easier way to do this but I can't think of one right now
function mergedArray(targets, toMerge) {
	var result = [];
	targets.forEach( function(t) { result.push(t); });
	toMerge.forEach( function(t) {
		if(t.salesforce_ref) {
			var o = _.find(result, { salesforce_ref: t.salesforce_ref });
			if(o) {
				// merge over values from t to o
				var k = Object.keys(t);
				k.forEach(function(kk) {
					if(kk === 'salesforce_ref' || kk === 'contributions') {
						return;
					}
					o[kk] = t[kk];
				});
			} else {
				result.push(t);
			}
		} else {
			result.push(t);
		}
	});
	return result;
}


// note we never remove keys only add or update
// the way we check is
// does the key even exist? 
//    if not just stick it in
// Is the object a string or number?
//    if it is then stick it in
// Is the object an array?
//    if it is then append the delta
// Is the object an object
//    then deep merge the sucker
function merge(target, delta) {
	for(var key in delta) {
		if(!target[key]) {
			target[key] = delta[key];
		} else {
			var type = typeof(delta[key]);
			if (/boolean|string|number/.test(type) || delta[key] === null) {
				target[key] = delta[key];
			} else if(delta[key].constructor === Array) {
				target[key] = mergedArray(target[key], delta[key]);
			} else {
				merge(target[key], delta[key]);
			}
		}
	}
}
module.exports.applyDelta = merge;

function dateConverter(val) {
	return (new Date(val)).valueOf();
}
function sfObjectToNameConverter(val) {
	return val.Name;
}

function makeDelta(result, A, B, keyA, keyB, converter) {

	if(typeof A[keyA] !== 'undefined') {
		var val = converter ? converter(A[keyA]) : A[keyA];
		if(!B[keyB] || val !== B[keyB]) {
			result[keyB] = val;
		}
	}
}

module.exports.acceleratorDelta = function(salesforce, couch) {
    var result = { };
    
    function checkVal(keySF, keyCB, converter) {
        makeDelta(result, salesforce, couch, keySF, keyCB, converter);
    }

	// valid document id
	if(!couch._id) {
		result._id = salesforce.Id;
	}

    if(!couch.salesforce_ref) {
        result.salesforce_ref = salesforce.Id;
    }
    checkVal('Accelerator_start_date__c', 'start_date', dateConverter);
    checkVal('Country__r', 'location', sfObjectToNameConverter);

    return result;
};

module.exports.projectDelta = function(salesforce, couch) {
	var result = { };
	
	function checkVal(keySF, keyCB, converter) {
		makeDelta(result, salesforce, couch, keySF, keyCB, converter);
	}

	// valid document id
	if(!couch._id) {
		result._id = salesforce.Id;
	}

	checkVal('Name', 'name');
	checkVal('CreatedDate', 'created_date', dateConverter);
	checkVal('Project_mission__c', 'mission');
	checkVal('Project_problem__c', 'problem');
	checkVal('Project_solution__c', 'solution');
	checkVal('Launched__c', 'launched');

  console.log('publish to website');
	checkVal('Publish_to_website__c', 'published');
  console.log(result);
  
	checkVal('Impact_category__r', 'category', sfObjectToNameConverter);
	checkVal('Country__r', 'location', sfObjectToNameConverter);

  // we're grabbing one value from cm and applying to project
  var sfCM = salesforce["Changemaker__r"];
  function checkCMValForProj(keySF, keyCB, converter) {
      makeDelta(result, sfCM, couch, keySF, keyCB, converter);
  }
  checkCMValForProj('Accelerator__c', 'accelerator_id');

	// deal with the changemaker object
	var cmDelta = { };
	var cbCM = couch.changemaker || { };
	function checkCMVal(keySF, keyCB, converter) {
		makeDelta(cmDelta, sfCM, cbCM, keySF, keyCB, converter);
	}
	checkCMVal("Id", "_id");
	checkCMVal("FirstName", "first_name");
	checkCMVal("LastName", "last_name");
	checkCMVal("Story__c", "story");
	checkVal("Changemaker__r", "changemaker_id", function(val) {
		return val.Id;
	});
	if(!cbCM.type) {
		cmDelta.type = "changemaker";
	}
	if(Object.keys(cmDelta).length) {
		result.changemaker = cmDelta;
	}

    // deal with the media object
    var mediaDelta = { };
    var cbMedia = couch.media || { };
    function checkMediaVal(keySF, keyCB, converter) {
        makeDelta(mediaDelta, salesforce, cbMedia, keySF, keyCB, converter);
    }
    checkMediaVal('Video_URL__c', 'video_url_main');
    if(Object.keys(mediaDelta).length) {
        result.media = mediaDelta;
    }

	return result;
};

module.exports.projectUpdatesDelta = function(salesforces, couch) {
	var result = { };
	var updates = [];
	if(!couch.updates) {
		couch.updates = [];
	}

	function toUpdate(sf) {
		return {
			salesforce_ref: sf.Id,
			title: sf.Name,
			body: sf.Update__c,
			created_date: sf.CreatedDate,
			modified_date: sf.LastModifiedDate
		};
	}
	
	updates = salesforces.map(toUpdate);

	if(updates.length) {
		result.updates = updates;
	}

	return result;
};

module.exports.fundingDelta = function(salesforces, couch) {
	var result = { };
	var fundingGrants = [];

	if(!couch.funding_grants) {
		couch.funding_grants = [];
	}

	function toFundingGrant(sf) {
		return {
			salesforce_ref: sf.Id,
			attained_date: sf.Date_attained_max__c || null,
			created_date: sf.CreatedDate,
			amount_usd: sf.donation_split__Maximum_Funds__c,
			contributions: []
		};
	}

	salesforces.forEach(function(sf) {
		var existingGrant = _.find(couch.funding_grants, { salesforce_ref: sf.Id });
		if(existingGrant) {
			//Date_attained_max__c == if it's been attained
			if(!existingGrant.attained_date && sf.Date_attained_max__c) {
				existingGrant.attained_date = sf.Date_attained_max__c;
				fundingGrants.push(existingGrant);
			}
		} else {
			var newGrant = toFundingGrant(sf);
			newGrant.received_usd = 0;
			fundingGrants.push(newGrant);
		}
	});

	if(fundingGrants.length) {
		result.funding_grants = fundingGrants;
	}
	return result;
};

module.exports.userDelta = function(salesforce, couch) {
	var result = { };
	
	function checkVal(keySF, keyCB, converter) {
		makeDelta(result, salesforce, couch, keySF, keyCB, converter);
	}

	if(!couch.salesforce_ref) {
		result.salesforce_ref = salesforce.Id;
	}
	checkVal('FirstName', 'first_name');
	checkVal('LastName', 'last_name');
	checkVal('Email', 'email');

	// for now we're not going to deal with the other fields

	return result;
};

