// This file produces the queries based on the object definition in salesforce
// I did this because it's just easier than having to work out relations
// and type all the fields by hand. -- sugendran

var nforce = require("./login");
var _ = require('lodash');
var async = require('async');
var fs = require('fs');
var oauth;
var sf;

function getSalesForceObjects(callback) {
    sf.getSObjects(oauth, function(err, resp) {
        if(err) {
            throw err;
        }
        console.log("all schemas: %j", _.map(resp.sobjects, 'name'));
        var allowed = ['Contact', 'RecordType', 'Opportunity','Campaign','OpportunityContactRole'];
        var ourObjs = _.filter(resp.sobjects, function(o) {
            return o.custom || allowed.indexOf(o.name) !== -1;
        });
        callback(ourObjs);
    });
}

function getObjectSchema(objName, callback) {
    sf.getDescribe(objName, oauth, callback);
}


function makeQueries(objs, callback) {
    var skips = ['CreatedById', 'OwnerId', 'LastModifiedById', 'Concat', 'AccountId', 'ReportsToId', 'MasterRecordId', 'Pricebook2Id'];
    function getSchema(schemaName) {
        var schema = objs[schemaName];
        if(!schema) {
            schema = _.find(objs, { name: schemaName });
        }
        if(!schema) {
            console.log(JSON.stringify(objs));
            throw new Error("schema not found for " + schemaName);
        }
        return schema;
    }
    function getFieldName(field) {
        if(skips.indexOf(field.name) !== -1) {
            return field.name;
        }
        if(field.relationshipName) {
            var schemas = _.map(field.referenceTo, getSchema);
            var result = [];
            _.each(schemas, function(schema) {
                result.push(_.map(schema.fields, function(f) {
                    return field.relationshipName + "." + f.name;
                }));
            });
            return _.flatten(result);
        }
        return field.name;
    }
    function getFieldsForSchema(schemaName) {
        var schema = getSchema(schemaName);
        var allfields = _.flatten(_.map(schema.fields, getFieldName));
        return allfields;
    }
    function queryForSchema(schemaName) {
        var aSchema = getSchema(schemaName);
        var aFields = getFieldsForSchema(schemaName);
        var aQuery = 'SELECT ' + aFields.join(",") + " FROM " + aSchema.name;
        return aQuery;
    }

    var acceleratorQuery = queryForSchema('Accelerator');
    var projectQuery = queryForSchema('Project');
    var peopleQuery = queryForSchema('People');
    var designationQuery = queryForSchema('donation_split__Designation__c');
    var budgetYearQuery = queryForSchema('donation_split__Budget_Year__c');
    var designationBudgetQuery = queryForSchema('donation_split__Designation_Budget__c');
    var opportunityQuery = queryForSchema('Opportunity');
    var recordTypes = queryForSchema('RecordType');
    var oppContactRole = queryForSchema('OpportunityContactRole');
    var projectUpdatesQuery = queryForSchema('Project_Update__c');

    var allQueries = {
        'accelerator': acceleratorQuery,
        'project': projectQuery,
        'people': peopleQuery,
        'designations': designationQuery,
        'designationBudgets': designationBudgetQuery,
        'budgetYears': budgetYearQuery,
        'opportunities': opportunityQuery,
        'opportunityContacts': oppContactRole,
        'recordTypes': recordTypes,
        'projectUpdates': projectUpdatesQuery
    };
    fs.writeFileSync(__dirname + '/queries.json', JSON.stringify(allQueries, null, 2));
    //sf.query(projectQuery, oauth, debugCallback);
}

nforce.connect(function(err, connectionDetails) {
    if(err) {throw err;}
    sf = connectionDetails.nforce;
    oauth = connectionDetails.oauth;

    // get the objects and create the queries
    getSalesForceObjects(function(objs) {
        //console.log("objects: %j", objs);
        var names = _.map(objs, 'name');
        console.log("object schemas are: %j", names);
        async.map(names, getObjectSchema, function(err, values) {
            var keys = _.map(values, 'label');
            console.log("keys are: %j", keys);
            var details = _.object(keys, values);
            //console.log("details: %j", details);
            makeQueries(details);
        });
    });
});

function debugCallback(err, resp) {
    if(err) console.error(err);
    if(resp) console.log(JSON.stringify(resp, null, "\t"));
}