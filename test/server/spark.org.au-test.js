"use strict";

var spark = require('../../lib/spark.org.au.js');

exports['awesome'] = {
  setUp: function(done) {
    // setup here
    done();
  },
  'no args': function(test) {
    test.expect(1);
    // tests here
    test.equal(spark.awesome(), 'awesome', 'should be awesome.');
    test.done();
  }
};
