Backend workflow
================

The application should look something like this

```
[controller] <-> [library] <-> [database]
```

---
## Library Layer

Business logic goes here!

### Password Hasher

The password hasher is fairly simple. It takes a password in and produces a 
salted hash with salt. The resulting object should then be saved in the database.
When we go to validate a password sent to us it will apply the hash and 
iterations values that we saved in the database to the inputted password. If
the resulting hash matches what we have in the database then the password is
valid.

---
## Database Layer

For this project we are using couchdb. If at a future date we discover that 
couchdb is not the appropriate database then it should be easy enough to 
replace this layer.


### Using the database


The file `lib/db.js` is what we will call the database interface. Anyone who 
needs to use the database should `require` it and then call the `.ready()` 
method on the module. The `.ready()` takes 1 parameter which is a callback 
function. It will ensure the database is bootstrapped and then call the callback
with an object that is the database api. The main reason for this is that it 
makes anything that uses the db module to be unit testable since we can mock the
apis. For more info on mocking files/modules see the 
[mockery project on github](https://github.com/mfncooper/mockery)


### Database bootstrap


The database bootstrap works like this:

 1. Create database if needed
 2. Create or Update database design views 
 3. Create the API object
 4. Set flags to mark everything as bootstrapped
 5. Callback to waiting callbacks
 
Once it has been bootstrapped any calls to `ready` will just be served the api.


### Coding in the database layer


The only logic that should live in the database layer is logic that validates 
the data being saved. Please keep all other logic in libs that the controllers 
consume. The big reason for that is unit testing, the smaller reason is that 
it means we don't end up with spaghetti.
