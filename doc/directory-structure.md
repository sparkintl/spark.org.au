Repository Layout
=================

Make sure you put your code in the correct spot or the 
[Grues](http://en.wikipedia.org/wiki/Grue_%28monster%29) will eat your face.

	* [dev](#dev)
	* [doc](#doc)
	* [lib](#lib)
	* [public](#public)
		* [images](#public_images)
		* [javascripts](#public_javascripts)
		* [stylesheets](#public_stylesheets)
	* [routes](#routes)
	* [test](#test)
		* [client](#test_client)
		* [server](#test_server)
		* [server-integration](#test_server_integration)
	* [views](#views)
	* [config.js](#config_js)
	* [index.js](#index_js)
	* [grunt.js](#grunt_js)

### <a name="dev">dev/</a>

Scripts and tools to use while developing the website, but that shouldn't be used in production.

### <a name="doc">doc/</a>

Any documents relevant to developing the website should be put in this folder

### <a name="lib">lib/</a>

This is where the application logic belongs. The controllers that render the 
website should be as thin as possible with all logic in the files located here.
By doing this then all the logic of the application can be unit tested.

### <a name="public">public/</a>

Any content placed in this folder will be served as static files.

### <a name="public_images">public/images/</a>

In the interest of keeping things neat please put all graphics used in the 
template/theme of the website in this folder

### <a name="public_javascripts">public/javascripts/</a>

All client side javascript should live in this folder. For now let's not
minify or concatenate the code as we may not need such a fancy system given
that most of the site is fairly static.

### <a name="public_stylesheets">public/stylesheets/</a>

This folder holds the [Stylus](http://learnboost.github.com/stylus/) files which
are used to generate the CSS files. If the filename is `foo.styl` then a request
for `foo.css` will cause `foo.styl` to be processed and outputed to `foo.css` 
which is then served in subsequent requests.

### <a name="routes">routes/</a>

This folder contains the controller code. As noted above, these controllers 
should be fairly thin. The `index.js` file in this folder has a `setup` method
that sets up all the routes.

### <a name="test">test/</a>

This folder contains all the test items

### <a name="test_client">test/client/</a>

This folder contains the client side [QUnit](http://qunitjs.com) tests

### <a name="test_server">test/server/</a>

This folder contains the [Nodeunit](https://github.com/caolan/nodeunit) unit 
tests for server code

### <a name="test_server_integration">test/server-integration/</a>

This folder contains the integration tests for the server

### <a name="views">views/</a>

This folder contains the views for rendering. The `layout.jade` file is the 
master template which exposes blocks for the other templates to render.

### <a name="config_js">config.js/</a>

This is the config file for the application. The file is in the gitignore so
if you wish to commit changes use the `-f` option when adding it to your commit.
Make sure that no keys or secrets are stored in this file, all such data should
come from environment variables set on the server.

### <a name="index_js">index.js/</a>

This is the launching point for the application. Anything that is outside of 
configuring the application does not belong here. If it's not configuration then
it can be tested, and testable things should be in the `lib` folder

### <a name="grunt_js">grunt.js/</a>

Grunt file for automation.

