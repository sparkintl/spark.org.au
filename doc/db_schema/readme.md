# About these schema documents

The schema documents in this folder follow the [JSON Schema draft spec](http://tools.ietf.org/html/draft-zyp-json-schema-03).

Each file is not necessarily a top-level document. Top-level document types are:

* project
* user
* accelerator
