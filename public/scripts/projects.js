/*globals $:false, window:false, Spark:false, document : false */
(function() {
    "use strict";

    var $projectForm = $('.project-search');
    $projectForm.on('change', 'input[type="radio"], input[type="checkbox"]', function() {
        $(this).siblings('input').addBack().each(function() {
            $(this).toggleClass('checked', this.checked);
        });
    }).on('change', ':input', function() {
        var country = $projectForm.find('[name="country"]:checked').val();
        var category = $projectForm.find('[name="category"]:checked').val();
        var order = $projectForm.find('[name="order"]').val();
        window.location = Spark.searchUrl(country, category, order);
    });

    $(document)
        .on('click', '.tabs > .tab-menu > .tab', function() {
            var $this = $(this);
            $this.addClass('selected');
            $this.siblings('.tab').removeClass('selected');
            var $pane = $this.closest('.tabs')
                            .find('> .tab-content > [aria-labelledby="' + $this.attr('id') + '"]');
            $pane.attr('aria-hidden', false);
            $pane.siblings('[role="tabpanel"]').attr('aria-hidden', true);
        })
        .on('change', '#project-actions select[name="fund_amount"]', function (e) {
            $('#project-actions .fund-button > .detail').text("$" + $(this).val());
        })
        .on('click', '#project-actions .fund-button', function(e) {
            e.preventDefault();
            var $toggledParent = $(this).closest('#project-actions');
            var $toggledOther = $toggledParent.siblings('#donation-form');

            var thisAmount = $toggledParent.find('[name="fund_amount"]').val();
            $toggledOther.find('[name="fund_amount"]').val(thisAmount);

            $toggledParent.attr('aria-hidden', 'true');
            $toggledOther.attr('aria-hidden', 'false');

            //ga('send', 'event', 'category', 'action');
            //ga('send', 'event', 'category', 'action', 'label');
            //ga('send', 'event', 'category', 'action', 'label', value);  // value is a number.
            ga('send', 'event', 'donation', 'started');
        })
        .on('click', '#donation-form .cancel, .donation-success .cancel', function(e) {
            e.preventDefault();
            var $toggledParent = $(this).closest('#donation-form, .donation-success');
            var $toggledOther = $toggledParent.siblings('#project-actions');

            $toggledParent.attr('aria-hidden', 'true');
            $toggledOther.attr('aria-hidden', 'false');

            ga('send', 'event', 'donation', 'cancelled');
        });

    $('#donation-form').on('submitCompleted', function(e, data, user) {
        var isFunded = data.goal_usd <= data.attained_usd;
        var $this = $(this);
        var $succeeded = $this.next('.donation-success');
        $this.attr('aria-hidden', 'true');
        $succeeded
            .find('.first-name').text(user.first).end()
            .find('.last-name').text(user.last).end()
            .find('.remaining-funder-count').text(Math.ceil((data.goal_usd - data.attained_usd) / 2500)).end()
            .find('.mission-accomplished').attr('aria-hidden', !isFunded).end()
            .find('.remaining-needed').attr('aria-hidden', isFunded).end();
        if (isFunded) {
            $succeeded.find('.share').text('Know others who can help? Spread the word');
        }
        $succeeded.attr('aria-hidden', 'false');

        var percent = 100 * data.attained_usd / data.goal_usd;
        $('.progress-statement > .percent-attained')
            .text(Math.floor(percent) + "%");
        $('.grant-info > .progress-bar > .filled-bar')
            .width(percent + '%');
        var $funders = $('.funder-count');
        var funderCount = parseInt($.trim($funders.first().text()));
        $funders.text(funderCount + 1);
    });
}());
