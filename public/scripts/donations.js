/*globals brainTreeEncryptionKey:false, csrfToken : false, Braintree:false, $:false*/
(function init() {
    "use strict";

    if (typeof Braintree === 'undefined') {
        setTimeout(init, 100);
        return;
    }

    var braintree = Braintree.create(brainTreeEncryptionKey);

    var $form = $('#donation-form');
    var $submit = $('#donation_submit');

    var validator = $form.validate();

    $form.submit(function() {
        // The braintree encryption duplicates the fields on a second submit.
        // even if validation fails.
        $form.find('[name="number"],[name="cvv"],[name="year"],[name="month"]').remove();
    });
    braintree.onSubmitEncryptForm('donation-form', function (e) {
        if (e.isDefaultPrevented()) {
            ga('send', 'event', 'donations', 'validation error');
            return;
        }
        e.preventDefault();

        $form.find(".form-error").remove();
        $submit.attr("disabled", "disabled");
        $.ajax({
            url : $form.attr('action'),
            type : 'POST',
            headers : {
                'x-csrf-token' : csrfToken
            },
            data : $form.serialize()
        }).always(function () {
            $submit.removeAttr("disabled");
        }).done(function(data) {
            console.log(data);
            $form.trigger('submitCompleted', [ data, {
                first : $('#donation_first_name').val(),
                last : $('#donation_last_name').val()
            } ]);
        }).fail(function(xhr) {
            var errorJson, $error, error, $field, field;
            try {
                errorJson = JSON.parse(xhr.responseText);
            } catch(e) {
                errorJson = xhr.responseText;
            }
            if (typeof errorJson === 'string') {
                $error = $('<div class="form-error error" />').text(errorJson);
                $('#donation_submit').after($error);
            } else if(errorJson.invalid) {
                for(field in errorJson.invalid) {
                    error = errorJson.invalid[field];
                    if (error instanceof Array) {
                        error = error.join(', ');
                    }
                    $field = $form.find('[name="' + field + '"], [data-encryption-name="' + field + '"]');
                    // copy the jquery.validation pattern - a label after the input
                    $error = $('<label for="' + $field.attr('id') + '" class="error" style="display: inline;" />');
                    $error.text(error);
                }
            } // else errorJson.missing or errorJson.excess - these shouldn't happen if JS is enabled.
        });
    });
}());
