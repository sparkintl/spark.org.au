"use strict";

var db = require("../lib/db");
var async = require('async');
var _ = require('lodash');
var slugs = require('slugs');
var config = require('../config');

var projectHelper = require('../lib/project-helper');

function getGrantStats(project) {
    var grant = projectHelper.getCurrentGrant(project);
    if (grant) {
        return {
            amount_usd : grant.amount_usd,
            received_usd : grant.received_usd,
            percent_attained : Math.floor(100 * (grant.received_usd / grant.amount_usd))
        };
    }
    return null;
}

function getFunders(project) {
    var funders = project.funders;
    var contributions = project.funding_grants
        .map(function(grant) {
            return grant.contributions;
        })
        .reduce(function(contributionList, newContributions) {
            return contributionList.concat(newContributions);
        }, []);

    return funders.map(function(funder) {
        var id = funder._id;
        var amount = contributions.reduce(function(sum, contribution) {
            if (contribution.user_id === id) {
                return sum + contribution.amount_usd;
            }
            return sum;
        }, 0);
        return {
            id : id,
            name : funder.first_name + ' ' + funder.last_name,
            location : funder.location,
            avatarUrl : funder.avatar_url,
            amount_usd : amount
        };
    });
}

function getHelpers(project) {
    return project.helpers.map(function(helper) {
        return {
            id : helper._id,
            name : helper.first_name + ' ' + helper.last_name,
            location : helper.location,
            avatarUrl : helper.avatar_url
        };
    });
}

function toProjectSearchEntry(project) {
    var searchImageUrl = '/images/projects/' + project.slug + '/search.jpg';
    if (project.media.image_url_search) {
        searchImageUrl = project.media.image_url_search;
    }
    return {
        name : project.name,
        mission : project.mission,
        url : '/change-maker/' + project.slug + '/',
        searchImageUrl : searchImageUrl,
        category : project.category,
        location : project.location,
        changemaker : project.changemaker,
        grant_info : getGrantStats(project)
    };
}

function toProjectDetail(project) {
    if (!project.changemaker.media.image_url_headshot) {
        project.changemaker.media.image_url_headshot = '/images/projects/' + project.slug + '/head.jpg';
    }
    if (!project.media.image_url_main) {
        project.media.image_url_main = '/images/projects/' + project.slug + '/main.jpg';
    }
    var acceleratedAt = project.created_date;
    // REMOVED AS PROBLEMATIC AND I DON'T HAVE TIME
    // if (project.accelerator.start_date) {
    //     acceleratedAt = project.accelerator.start_date;
    // }
    var monthNames = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
    acceleratedAt = new Date(acceleratedAt);
    acceleratedAt = monthNames[acceleratedAt.getMonth() + 1] + ' ' + acceleratedAt.getFullYear();
    return _.extend({
        categorySlug : slugs(project.category),
        locationSlug : slugs(project.location),
        grant_info : getGrantStats(project),
        funders : getFunders(project),
        helpers : getHelpers(project),
        acceleratedAt: acceleratedAt
    }, project);
}

var sortOrders = [
    { param : 'funding',          text : 'Almost funded first',    dbkey : 'goalDistance' },
    { param : 'newest',           text : 'Recently added first',   dbkey : 'creationDateDesc' }
    // { param : 'recently-updated', text : 'Recently updated first', dbkey : 'recentUpdateDesc' }
];
var sortOrderOptions = sortOrders.map(function(order) {
    return { key : order.param, value : order.text };
});
var sortOrderParamToDbKey = sortOrders.reduce(function(memo, order) {
    memo[order.param] = order.dbkey;
    return memo;
}, {});

function asSluggedFilterOptions(namesObj) {
    return Object.keys(namesObj).map(function(name) {
        return { key : slugs(name), value : name };
    }).concat([ { key : '', value : 'Any area' } ]);
}

function sumOfValues(obj) {
    var sum = 0;
    for(var prop in obj) {
        sum += obj[prop];
    }
    return sum;
}

function searchProjects(filters, db, next) {
    var country = filters.country;
    var category = filters.category;
    var order = sortOrderParamToDbKey[filters.order || 'funding'];
    var page = Number(filters.page || 0);

    db.project.search({
        location : country || null,
        category : category || null,
        order : order,
        start : page * 4
    }, function(err, result) {
        if (err) {
            return next(err);
        }
        next(null, {
            projects : result.projects,
            thisPage : page,
            totalPages : Math.ceil(result.total / 4)
        });
    });
}

function renderSearchPage(url, data, blankProject, res) {
    var projectsPage = Object.create(data.projectsPage);
    projectsPage.projects = data.projectsPage.projects.map(toProjectSearchEntry);

    var urlSansPaging = require('../shared/scripts/urls').searchUrl(
        data.selectedCountry,
        data.selectedCategory,
        data.selectedOrder);
    var pagingUrl = urlSansPaging + (~urlSansPaging.indexOf('?') ? '&page=' : '?page=');

    res.render("projects", {
        title : '',
        pagingUrl : pagingUrl,
        selectedCountry : data.selectedCountry || '',
        selectedCategory: data.selectedCategory || '',
        selectedOrder : data.selectedOrder || 'funding',
        countryFilters : data.countryFilters,
        categoryFilters : data.categoryFilters,
        orderFilters : data.orderFilters,
        projectsPage : projectsPage
    });
}

function handleSearchQuery(countrySlug, categorySlug, req, res, next) {
    db.ready(function (api) {
        var countryFilters = res.locals.countries
                                .concat([ { key : '', value : 'Any area' } ]);
        var categoryFilters = res.locals.categories
                                .concat([ { key : '', value : 'Any area' } ]);

        var selectedCountry = countryFilters.filter(function(c) {
            return c.key === countrySlug;
        })[0];
        var selectedCategory = categoryFilters.filter(function(c) {
            return c.key === categorySlug;
        })[0];

        searchProjects({
            country : selectedCountry && selectedCountry.value,
            category : selectedCategory && selectedCategory.value,
            page : req.query.page && (req.query.page - 1), // switch 1-based index to 0-based
            order : req.query.order
        }, api, function(err, projectsPage) {
            if (err) {
                return next(err);
            }
            projectsPage.thisPage += 1; // switch 0-based back to 1-based
            renderSearchPage(req.url, {
                countryFilters : countryFilters,
                categoryFilters : categoryFilters,
                orderFilters : sortOrderOptions,
                projectsPage : projectsPage,
                selectedCountry : selectedCountry && selectedCountry.key,
                selectedCategory : selectedCategory && selectedCategory.key,
                selectedOrder : req.query.order
            }, api.project.blank(), res);
        });
    });
}

/*
 * GET project listings.
 */
function allProjects(req, res, next) {
    handleSearchQuery(null, null, req, res, next);
}
module.exports.list = allProjects;

function allProjectsByCountry(req, res, next) {
    handleSearchQuery(req.params.countrySlug, null, req, res, next);
}
module.exports.listByCountry = allProjectsByCountry;

function allProjectsByCategory(req, res, next) {
    handleSearchQuery(null, req.params.categorySlug, req, res, next);
}
module.exports.listByCategory = allProjectsByCategory;

function allProjectsByCountryAndCategory(req, res, next) {
    handleSearchQuery(req.params.countrySlug, req.params.categorySlug, req, res, next);
}
module.exports.listByCountryAndCategory = allProjectsByCountryAndCategory;

function getProject(req, res, next) {
    var slug = req.params.projectSlug;
    if (!slug) {
        return res.send(404);
    }
    db.ready(function(api) {
        api.project.get(slug, function(err, project) {
            if (err) {
                return next(err);
            }
            if (!project) {
                return res.send(404);
            }
            console.log('HERE', res.locals.changemakerCount)
            res.render("projectDetail", {
                title : project.name,
                user : req.user,
                project : toProjectDetail(project),
                braintreeEncryptionKey : config.braintree.clientSideEncryptionKey,
                donationCountries : require('../lib/countries.json'),
                facebookAppId : config.facebook.clientID
            });
        });
    });
}
module.exports.getProject = getProject;

module.exports.donation = require('./project-donation');
